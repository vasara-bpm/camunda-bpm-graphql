# https://github.com/nmattia/niv
{ sources ? import ./sources.nix }:

let

  overlay = _: pkgs: {

    gitignoreSource = (import sources.gitignore {
      inherit (pkgs) lib;
    }).gitignoreSource;

  };

  pkgs = import sources.nixpkgs {
    overlays = [ overlay ];
    config = {};
  };

in pkgs
