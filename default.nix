{ pkgs ? import ./nix {},
}:

let

  mavenRepository = pkgs.stdenv.mkDerivation {
    name = "maven-dependencies";
    buildInputs = with pkgs;with pkgs;  [ jdk11_headless maven ];
    src = ./.;
    buildPhase = ''
      while mvn package -Dmaven.repo.local=$out -Dmaven.wagon.rto=5000; [ $? = 1 ]; do
        echo "timeout, restart maven to continue downloading"
     done
    '';
    # keep only *.{pom,jar,sha1,nbm} and delete all ephemeral files with lastModified timestamps inside
    installPhase = ''
      find $out -type f -regex '.+\\(\\.lastUpdated\\|resolver-status\\.properties\\|_remote\\.repositories\\)' -delete
    '';
    outputHashAlgo = "sha256";
    outputHashMode = "recursive";
    outputHash = "1z5mh7zh4k6jf1yjx08n4hiaiibl9ds1qz2iw847pgzpq0ngfydy";
  };

in

pkgs.stdenv.mkDerivation rec {
  pname = "camunda-bpm-graphql";
  version = "0.4.0-SNAPSHOT";
  name = "${pname}-${version}.jar";
  src = pkgs.lib.cleanSource ./.;

  buildInputs = with pkgs; [ jdk11_headless maven ];
  buildPhase = ''
    find . -print0|xargs -0 touch
    mvn package --offline -Dmaven.repo.local=${mavenRepository}
  '';

  installPhase = ''
    cp extension/target/${name} $out
    jar i $out
  '';
}
