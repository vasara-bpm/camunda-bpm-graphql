{ pkgs ? import ./nix {}
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    gnumake
    jdk11_headless
    maven
  ];
}
