package org.camunda.bpm.extension.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.apache.commons.io.IOUtils;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.DecisionDefinition;
import org.camunda.bpm.engine.rest.util.ApplicationContextPathUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class DecisionDefinitionResolver implements GraphQLResolver<DecisionDefinition> {

    @Autowired
    ProcessEngine processEngine;

    @Autowired
    RepositoryService repositoryService;


    public DecisionDefinitionResolver() {
    }

    public String getId(DecisionDefinition decisionDefinition) {
        return decisionDefinition.getId();
    }

    public String getKey(DecisionDefinition decisionDefinition) {
        return decisionDefinition.getKey();
    }

    public String getCategory(DecisionDefinition decisionDefinition) {
        return decisionDefinition.getCategory();
    }

    public String getName(DecisionDefinition decisionDefinition) {
        return decisionDefinition.getName();
    }

    public Integer getVersion(DecisionDefinition decisionDefinition) {
        return decisionDefinition.getVersion();
    }

    public String getResource(DecisionDefinition decisionDefinition) {
        return decisionDefinition.getResourceName();
    }

    public String getDeploymentId(DecisionDefinition decisionDefinition) {
        return decisionDefinition.getDeploymentId();
    }

    public String getDiagram(DecisionDefinition decisionDefinition) {
        InputStream inputStream = repositoryService.getDecisionModel(decisionDefinition.getId());
        try {
            return IOUtils.toString(inputStream, "UTF-8");
        }
        catch (IOException ex) {
            return "";
        }
    }

    public String contextPath(DecisionDefinition decisionDefinition) {
        String ddid = decisionDefinition.getDeploymentId();
        if (ddid != null) {
            return ApplicationContextPathUtil.getApplicationPathForDeployment(processEngine, ddid);
        } else
            return null;
    }
}
