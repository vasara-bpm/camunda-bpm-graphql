package org.camunda.bpm.extension.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.rest.util.ApplicationContextPathUtil;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.StartEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.apache.commons.io.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * Created by danielvogel on 20.06.17.
 */

@Component
public class ProcessDefinitionResolver implements GraphQLResolver<ProcessDefinition> {

    @Autowired
    ProcessEngine processEngine;

    @Autowired
    RepositoryService repositoryService;


    public ProcessDefinitionResolver() {
    }

    public String getId(ProcessDefinition processDefinition) {
        return processDefinition.getId();
    }

    public String getKey(ProcessDefinition processDefinition) {
        return processDefinition.getKey();
    }

    public String getCategory(ProcessDefinition processDefinition) {
        return processDefinition.getCategory();
    }

    public String getDescription(ProcessDefinition processDefinition) {
        return processDefinition.getDescription();
    }

    public String getName(ProcessDefinition processDefinition) {
        return processDefinition.getName();
    }

    public Integer getVersion(ProcessDefinition processDefinition) {
        return processDefinition.getVersion();
    }

    public String getResource(ProcessDefinition processDefinition) {
        return processDefinition.getResourceName();
    }

    public String getDeploymentId(ProcessDefinition processDefinition) {
        return processDefinition.getDeploymentId();
    }

    public String getDiagram(ProcessDefinition processDefinition) {
        InputStream inputStream = repositoryService.getProcessModel(processDefinition.getId());
        try {
            return IOUtils.toString(inputStream, "UTF-8");
        }
        catch (IOException ex) {
            return "";
        }
    }

    public Boolean getSuspended(ProcessDefinition processDefinition) {
        return processDefinition.isSuspended();
    }

    public String startFormKey(ProcessDefinition processDefinition) {
        BpmnModelInstance bpmnModelInstance = repositoryService.getBpmnModelInstance(processDefinition.getId());
        Collection<StartEvent> startEvents = bpmnModelInstance.getModelElementsByType(StartEvent.class);
        StartEvent startEvent = startEvents.iterator().next();
        return startEvent.getCamundaFormKey();
    }

    public String contextPath(ProcessDefinition processDefinition) {
        String pdid = processDefinition.getId();
        if (pdid != null) {
            return ApplicationContextPathUtil.getApplicationPathByProcessDefinitionId(processEngine, pdid);
        } else
            return null;
    }
}
