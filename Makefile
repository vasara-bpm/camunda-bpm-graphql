CACHIX_CACHE = vasara-bpm

PNAME ?= camunda-bpm-graphql
VERSION ?= 0.4.0-SNAPSHOT
OUTPUT ?= ${PNAME}-${VERSION}.jar

${OUTPUT}:
	nix-build --no-out-link|xargs -Iresult cp -L result ${OUTPUT}

build: ${OUTPUT}

.PHONY: cache
cache:
	nix-store --query --references $$(nix-instantiate shell.nix) --references $$(nix-instantiate default.nix) | \
	xargs nix-store --realise | xargs nix-store --query --requisites | cachix push $(CACHIX_CACHE)

.PHONY: shell
shell:
	nix-shell

###

.PHONY: nix-%
nix-%: .netrc
	@echo "run inside nix-shell: $*"
	nix-shell--run "$(MAKE) $*"
